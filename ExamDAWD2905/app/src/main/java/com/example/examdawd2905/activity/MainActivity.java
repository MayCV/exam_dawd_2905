package com.example.examdawd2905.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.examdawd2905.R;
import com.example.examdawd2905.database.DBHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edName;
    private EditText edQty;
    private Button btAdd;
    private Button btView;
    private DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        db = new DBHelper(this);
        db.getReadableDatabase();
    }

    private void initView(){
        edName = (EditText) findViewById(R.id.edName);
        edQty = (EditText) findViewById(R.id.edQty);
        btAdd = (Button) findViewById(R.id.btAdd);
        btView = (Button) findViewById(R.id.btView);
        btAdd.setOnClickListener(this);
        btView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btAdd:
                onAdd();
                break;
            case R.id.btView:
                onView();
                break;
            default:
                break;
        }
    }

    private void onAdd(){
        if (edName.getText().toString().isEmpty()){
            Toast.makeText(this,"Please enter product name", Toast.LENGTH_LONG).show();
            return;
        }

        if (edQty.getText().toString().isEmpty()){
            Toast.makeText(this,"Please enter quantity", Toast.LENGTH_LONG).show();
            return;
        }
        String isAdd = db.addProduct(edName.getText().toString(), edQty.getText().toString());
        Toast.makeText(this, isAdd, Toast.LENGTH_SHORT).show();
    }

    private void onView(){
        Intent intent = new Intent(MainActivity.this, ListProductAct.class);
        startActivity(intent);
    }
}
